/**
 * =============================================================================
 * Copyright(c) MagnaSolve LLC, 2019.
 * All rights reserved. 
 * =============================================================================
 */

package com.magnasolve.poc.service.template;

import java.io.FileNotFoundException;
import java.io.InputStream;

import com.magnasolve.poc.pdf.entity.DocumentTemplate;

//------------------------------------------------------------------------------
/**
 * Interface that defines the template service.
 * 
 * @author davidteagle
 */
//------------------------------------------------------------------------------
public interface TemplateService
{

    //--------------------------------------------------------------------------
    /**
     * @param templateId
     * @return
     */
    DocumentTemplate getTemplate( Long templateId );

    //--------------------------------------------------------------------------
    /**
     * @param template
     * @return
     */
    DocumentTemplate addTemplate( DocumentTemplate template );

    //--------------------------------------------------------------------------
    /**
     * @param templateFile
     * @return
     */
    DocumentTemplate addTemplateFromFile( String name, InputStream templateFile );
    
    //--------------------------------------------------------------------------
    /**
     * @param template
     * @return
     */
    DocumentTemplate updateTemplate( DocumentTemplate template );

    //--------------------------------------------------------------------------
    /**
     * @param templateId
     * @return
     */
    Integer deleteTemplate( Long templateId );

    //--------------------------------------------------------------------------
    /**
     * @param templateId
     * @return
     * @throws FileNotFoundException 
     */
    InputStream getTemplateSource( Long templateId ) throws FileNotFoundException;

}