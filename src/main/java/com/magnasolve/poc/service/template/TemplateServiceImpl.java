/**
 * =============================================================================
 * Copyright(c) MagnaSolve LLC, 2019.
 * All rights reserved. 
 * =============================================================================
 */
package com.magnasolve.poc.service.template;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.magnasolve.poc.pdf.entity.DocumentTemplate;
import com.magnasolve.poc.pdf.entity.DocumentTemplateField;
import com.magnasolve.poc.pdf.repository.DocumentTemplateRepository;
import com.magnasolve.poc.pdf.util.PdfUtil;

//------------------------------------------------------------------------------
/**
 * @author davidteagle
 *
 */
//------------------------------------------------------------------------------
@Service
public class TemplateServiceImpl implements TemplateService 
{
	@Autowired
	DocumentTemplateRepository templateRepository;
	
	@Value("${pdf.template.directory}")
	private String templateDirectory;
	
	//--------------------------------------------------------------------------
    /* (non-Javadoc)
     * @see com.magnasolve.poc.service.template.TemplateService#getTemplate(java.lang.Long)
     */
	@Override
    public DocumentTemplate getTemplate(Long templateId)
	{
	    Optional<DocumentTemplate> template = templateRepository.findById( templateId );
	    
	    if( template.isPresent() ) return template.get();
	    
		return null;
	}
	
	//--------------------------------------------------------------------------
    /* (non-Javadoc)
     * @see com.magnasolve.poc.service.template.TemplateService#addTemplate(com.magnasolve.poc.pdf.model.DocumentTemplate)
     */
	@Override
    public DocumentTemplate addTemplate( DocumentTemplate template )
	{
		return null;
	}
	
	//--------------------------------------------------------------------------
	/* (non-Javadoc)
	 * @see com.magnasolve.poc.service.template.TemplateService#addTemplateFromFile(java.io.File)
	 */
	@Override
    public DocumentTemplate addTemplateFromFile( String name, InputStream templateFile )
    {
        String fileName = templateDirectory + name + ".pdf";
        
        PDDocument document = PdfUtil.documentFromStream( templateFile );
        
        List<String> fieldNames = PdfUtil.getPdfFieldNames( document );
        
        DocumentTemplate template = new DocumentTemplate();
        template.setTemplateName( name );
        template.setTemplateType( 2 );
        template.setFilePath( fileName );
        
        fieldNames.forEach( fieldName -> 
        {  
            template.addTemplateField( new DocumentTemplateField( fieldName, "" ) );
        });
        
        templateRepository.save( template );
        
        try
        {
            document.save( fileName );
        } 
        catch ( IOException e )
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        return template;
    }
	
	//--------------------------------------------------------------------------
    /* (non-Javadoc)
     * @see com.magnasolve.poc.service.template.TemplateService#updateTemplate(com.magnasolve.poc.pdf.model.DocumentTemplate)
     */
	@Override
    public DocumentTemplate updateTemplate( DocumentTemplate template )
	{
		return null;
	}
	
	//--------------------------------------------------------------------------
    /* (non-Javadoc)
     * @see com.magnasolve.poc.service.template.TemplateService#deleteTemplate(java.lang.Long)
     */
	@Override
    public Integer deleteTemplate( Long templateId )
	{
		return 0;
	}
	
	//--------------------------------------------------------------------------
    /* (non-Javadoc)
     * @see com.magnasolve.poc.service.template.TemplateService#retrieveTemplateSource(java.lang.Long)
     */
	@Override
    public InputStream getTemplateSource( Long templateId ) throws FileNotFoundException
	{
	    DocumentTemplate template = getTemplate( templateId );
	    
	    if (template != null && template.getFilePath() != null )
	    {
	        return new FileInputStream( template.getFilePath() );        
	    }
	    return null;
	}

}
