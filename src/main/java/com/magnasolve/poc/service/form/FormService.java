/**
 * =============================================================================
 * Copyright(c) MagnaSolve LLC, 2019.
 * All rights reserved. 
 * =============================================================================
 */

package com.magnasolve.poc.service.form;

import java.io.InputStream;

import com.magnasolve.poc.pdf.entity.FormData;

//------------------------------------------------------------------------------
/**
 * Interface that defines a Form service.
 * @author davidteagle
 *
 */
//------------------------------------------------------------------------------
public interface FormService
{

    //--------------------------------------------------------------------------
    /**
     * @param data
     * @return
     */
    FormData saveFormData( FormData data );

    //--------------------------------------------------------------------------
    /**
     * @param file
     * @return
     */
    FormData saveFormDataFromFile( InputStream file );
    
    //--------------------------------------------------------------------------
    /**
     * @param data
     * @return
     */
    FormData updateFormData( FormData data );

    //--------------------------------------------------------------------------
    /**
     * @param ownerId
     * @param templateId
     * @return
     */
    FormData getFormData( Long ownerId, Long templateId );

    //--------------------------------------------------------------------------
    /**
     * @param id
     * @return
     */
    void deleteFormData( Long id );

}