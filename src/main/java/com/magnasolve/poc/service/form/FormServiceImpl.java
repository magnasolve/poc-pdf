/**
 * =============================================================================
 * Copyright(c) MagnaSolve LLC, 2019.
 * All rights reserved. 
 * =============================================================================
 */
package com.magnasolve.poc.service.form;

import java.io.InputStream;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.magnasolve.poc.pdf.entity.FormData;
import com.magnasolve.poc.pdf.repository.FormDataRepository;

//------------------------------------------------------------------------------
/**
 * @author davidteagle
 *
 */
//------------------------------------------------------------------------------
@Service
public class FormServiceImpl implements FormService
{
    @Autowired
    private FormDataRepository repository;
    
    //--------------------------------------------------------------------------
    /* (non-Javadoc)
     * @see com.magnasolve.poc.service.form.FormService#saveFormData(com.magnasolve.poc.pdf.model.FormData)
     */
    @Override
    public FormData saveFormData( FormData data )
    {
        return repository.save( data );
    }
    
    //--------------------------------------------------------------------------
    /* (non-Javadoc)
     * @see com.magnasolve.poc.service.form.FormService#saveFormDataFromFile(java.io.InputStream)
     */
    @Override
    public FormData saveFormDataFromFile( InputStream file )
    {
        // TODO Auto-generated method stub
        return null;
    }
    
    //--------------------------------------------------------------------------
    /* (non-Javadoc)
     * @see com.magnasolve.poc.service.form.FormService#updateFormData(com.magnasolve.poc.pdf.model.FormData)
     */
    @Override
    public FormData updateFormData( FormData data )
    {
        return repository.save( data );
    }
    
    //--------------------------------------------------------------------------
    /* (non-Javadoc)
     * @see com.magnasolve.poc.service.form.FormService#getFormData(java.lang.Long, java.lang.Long)
     */
    @Override
    public FormData getFormData( Long ownerId, Long templateId )
    {
        Optional<FormData> data = 
            repository.findDistinctByDataOwnerIdAndTemplateId( ownerId, templateId );
        
        if( data.isPresent() )
        {
            return data.get();
        }
        return null;
    }
    
    //--------------------------------------------------------------------------
    /* (non-Javadoc)
     * @see com.magnasolve.poc.service.form.FormService#deleteFormData(java.lang.Long)
     */
    @Override
    public void deleteFormData( Long id )
    {
        if( repository.existsById( id ))
        {
            repository.deleteById( id );
        }
    }

    
}
