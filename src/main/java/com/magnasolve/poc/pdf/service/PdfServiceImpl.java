package com.magnasolve.poc.pdf.service;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.List;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.fdf.FDFDocument;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.magnasolve.poc.pdf.entity.DocumentTemplate;
import com.magnasolve.poc.pdf.entity.FormData;
import com.magnasolve.poc.pdf.entity.FormDataEntry;
import com.magnasolve.poc.pdf.util.PdfUtil;
import com.magnasolve.poc.pdf.util.TemplateUtil;
import com.magnasolve.poc.service.form.FormService;
import com.magnasolve.poc.service.template.TemplateService;

//------------------------------------------------------------------------------
/**
 * @author dteagle
 *
 */
//------------------------------------------------------------------------------
@Service
public class PdfServiceImpl implements PdfService
{
    @Autowired
    private TemplateService templateService;
    
    @Autowired 
    private FormService formService;
    
    @Value("${pdf.output.directory}")
    private String outputDirectory;
    
    //--------------------------------------------------------------------------
    /* (non-Javadoc)
     * @see com.magnasolve.poc.pdf.service.PdfService#loadPdfDocument(java.lang.Long)
     */
    @Override
    public PDDocument loadPdfTemplate( Long templateId ) 
    {      
        PDDocument document = null;
      
        try
        {
            document = PDDocument.load( templateService.getTemplateSource( templateId ));
        } 
        catch ( Exception e )
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }       
        return document;
    }

    //--------------------------------------------------------------------------
    /* (non-Javadoc)
     * @see com.magnasolve.poc.pdf.service.PdfService#loadPdfFormData(java.io.InputStream)
     */
    @Override
    public FormData loadPdfFormData( FormData formData, InputStream stream )
    {
        PDDocument document = PdfUtil.documentFromStream( stream );
        
        DocumentTemplate template = templateService.getTemplate( formData.getTemplateId() );
        
        List<PDField> fields = PdfUtil.getPdfFields( document );
        
        fields.forEach( field -> 
        {
            if( TemplateUtil.verifyFieldName( field.getPartialName(), template ))
            {
                formData.addFormDataEntry( new FormDataEntry(
                    field.getPartialName(), field.getValueAsString() ));
            }
        });
        return formService.saveFormData( formData );
    }
    
    //--------------------------------------------------------------------------
    /* (non-Javadoc)
     * @see com.magnasolve.poc.pdf.service.PdfService#getXfdfTemplate(java.lang.Long)
     */
    @Override
    public String getXfdfTemplate( Long templateId )
    {
        StringWriter writer = new StringWriter();
        
        PDDocument document = loadPdfTemplate( templateId );
        
        if( document != null )
        {
            FDFDocument fdf = PdfUtil.getFDF( document );
            try
            {
                fdf.writeXML( writer );
                return writer.toString();
            } 
            catch ( IOException e )
            {
                return "error: " + e.getLocalizedMessage();
            }
            
        }
        return "error: No template found.";
    }
    
    //--------------------------------------------------------------------------
    /* (non-Javadoc)
     * @see com.magnasolve.poc.pdf.service.PdfService#generatePdf(java.lang.Long, java.lang.Long)
     */
    @Override
    public String generatePdf( Long dataOwnerId, Long templateId )
    {
        PDDocument document = loadPdfTemplate( templateId );
        FormData data = formService.getFormData( dataOwnerId, templateId );
        
        if( document != null && data != null )
        {
            PDDocument merge = PdfUtil.generateMergedForm( document, data );
            String fileName = outputDirectory + "form-" + templateId + "-" + dataOwnerId + ".pdf";
            try
            {
                merge.save( fileName );
                return fileName;
            } 
            catch ( IOException e )
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return "error: " + e.getLocalizedMessage();
            } 
        }
        return "error: " + "No Document.";
    }
    
    //--------------------------------------------------------------------------
    /* (non-Javadoc)
     * @see com.magnasolve.poc.pdf.service.PdfService#generateFdf(java.lang.Long, java.lang.Long)
     */
    @Override
    public String generateXfdf( Long dataOwnerId, Long templateId )
    {
        // TODO not implemented
        return null;
    }
    
}
