package com.magnasolve.poc.pdf.service;

import java.io.InputStream;

import org.apache.pdfbox.pdmodel.PDDocument;

import com.magnasolve.poc.pdf.entity.FormData;

public interface PdfService
{

    PDDocument loadPdfTemplate( Long templateId );
    
    FormData loadPdfFormData( FormData formData, InputStream stream );
    
    String getXfdfTemplate( Long templateId );

    String generatePdf( Long dataOwnerId, Long templateId );

    String generateXfdf( Long dataOwnerId, Long templateId );

}