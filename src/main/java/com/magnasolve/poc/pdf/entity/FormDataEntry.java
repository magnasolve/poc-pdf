/**
 * =============================================================================
 * Copyright(c) MagnaSolve LLC, 2019.
 * All rights reserved. 
 * =============================================================================
 */
package com.magnasolve.poc.pdf.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

//------------------------------------------------------------------------------
/**
 * @author dteagle
 *
 */
//------------------------------------------------------------------------------
@Entity
@Table(name = "form_data_entry")
public class FormDataEntry
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "form_data_id")
    private FormData formData;
    
    private String entryKey;
    
    private String entryValue;
    
    //--------------------------------------------------------------------------
    /**
     */
    public FormDataEntry()
    {
        super();
    }

    //--------------------------------------------------------------------------
    /**
     * @param entryKey
     * @param entryValue
     */
    public FormDataEntry( String entryKey, String entryValue )
    {
        super();
        this.entryKey = entryKey;
        this.entryValue = entryValue;
    }

    //--------------------------------------------------------------------------
    /**
     * @return the id
     */
    public Long getId()
    {
        return id;
    }

    //--------------------------------------------------------------------------
    /**
     * @param id the id to set
     */
    public void setId( Long id )
    {
        this.id = id;
    }

    //--------------------------------------------------------------------------
    /**
     * @return the formData
     */
    public FormData getFormData()
    {
        return formData;
    }

    //--------------------------------------------------------------------------
    /**
     * @param formData the formData to set
     */
    public void setFormData( FormData formData )
    {
        this.formData = formData;
    }

    //--------------------------------------------------------------------------
    /**
     * @return the entryKey
     */
    public String getEntryKey()
    {
        return entryKey;
    }

    //--------------------------------------------------------------------------
    /**
     * @param entryKey the entryKey to set
     */
    public void setEntryKey( String entryKey )
    {
        this.entryKey = entryKey;
    }

    //--------------------------------------------------------------------------
    /**
     * @return the entryValue
     */
    public String getEntryValue()
    {
        return entryValue;
    }

    //--------------------------------------------------------------------------
    /**
     * @param entryValue the entryValue to set
     */
    public void setEntryValue( String entryValue )
    {
        this.entryValue = entryValue;
    }
}
