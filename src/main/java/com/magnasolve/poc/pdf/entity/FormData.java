/**
 * =============================================================================
 * Copyright(c) MagnaSolve LLC, 2019.
 * All rights reserved. 
 * =============================================================================
 */

package com.magnasolve.poc.pdf.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

//------------------------------------------------------------------------------
/**
 * @author dteagle
 *
 */
//------------------------------------------------------------------------------
@Entity
@Table(name = "form_data")
public class FormData
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    private Long dataOwnerId;

    private Long templateId;

    @OneToMany( mappedBy = "formData", cascade = CascadeType.ALL, orphanRemoval = true )
    private List<FormDataEntry> formDataEntries = new ArrayList<FormDataEntry>();
    
    
    //--------------------------------------------------------------------------
    /**
     */
    public FormData()
    {
        super();
    }

    
    //--------------------------------------------------------------------------
    /**
     * @param dataOwnerId
     * @param templateId
     */
    public FormData( Long dataOwnerId, Long templateId )
    {
        super();
        this.dataOwnerId = dataOwnerId;
        this.templateId = templateId;
    }

    //--------------------------------------------------------------------------
    /**
     * @return the id
     */
    public Long getId()
    {
        return id;
    }

    //--------------------------------------------------------------------------
    /**
     * @param id the id to set
     */
    public void setId( Long id )
    {
        this.id = id;
    }

    // --------------------------------------------------------------------------
    /**
     * @return the dataOwnerId
     */
    public Long getDataOwnerId()
    {
        return dataOwnerId;
    }

    // --------------------------------------------------------------------------
    /**
     * @param dataOwnerId the dataOwnerId to set
     */
    public void setDataOwnerId( Long dataOwnerId )
    {
        this.dataOwnerId = dataOwnerId;
    }

    // --------------------------------------------------------------------------
    /**
     * @return the templateId
     */
    public Long getTemplateId()
    {
        return templateId;
    }

    // --------------------------------------------------------------------------
    /**
     * @param templateId the templateId to set
     */
    public void setTemplateId( Long templateId )
    {
        this.templateId = templateId;
    }

    //--------------------------------------------------------------------------
    /**
     * @return the formDataEntries
     */
    public List<FormDataEntry> getFormDataEntries()
    {
        return formDataEntries;
    }

    //--------------------------------------------------------------------------
    /**
     * @param formDataEntries the formDataEntries to set
     */
    public void setFormDataEntries( List<FormDataEntry> formDataEntries )
    {
        this.formDataEntries = formDataEntries;
    }

    public void addFormDataEntry( FormDataEntry entry )
    {
        entry.setFormData( this );
        formDataEntries.add( entry );
    }
    
    public void removeFormDataEntry( FormDataEntry entry )
    {
        formDataEntries.remove( entry );
        entry.setFormData( null );
    }
}
