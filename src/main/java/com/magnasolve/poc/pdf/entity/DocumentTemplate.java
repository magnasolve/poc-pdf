/**
 * =============================================================================
 * Copyright(c) MagnaSolve LLC, 2019.
 * All rights reserved. 
 * =============================================================================
 */

package com.magnasolve.poc.pdf.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

//------------------------------------------------------------------------------
/**
 * @author davidteagle
 *
 */
//------------------------------------------------------------------------------
@Entity
@Table(name = "document_template")
public class DocumentTemplate 
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    private String templateName;
    
    private int templateType;
    
    private String filePath;	
    
    @OneToMany( mappedBy = "template", cascade = CascadeType.ALL, orphanRemoval = true )
    private List<DocumentTemplateField> templateFields = new ArrayList<DocumentTemplateField>();
    
	//--------------------------------------------------------------------------
    /**
     * @return the id
     */
    public Long getId()
    {
        return id;
    }

    //--------------------------------------------------------------------------
    /**
     * @param id the id to set
     */
    public void setId( Long id )
    {
        this.id = id;
    }

    //--------------------------------------------------------------------------
    /**
     * @return the templateName
     */
    public String getTemplateName()
    {
        return templateName;
    }

    //--------------------------------------------------------------------------
    /**
     * @param templateName the templateName to set
     */
    public void setTemplateName( String templateName )
    {
        this.templateName = templateName;
    }

    //--------------------------------------------------------------------------
    /**
     * @return the type
     */
    public int getTemplateType()
    {
        return templateType;
    }

    //--------------------------------------------------------------------------
    /**
     * @param type the type to set
     */
    public void setTemplateType( int type )
    {
        this.templateType = type;
    }

    //--------------------------------------------------------------------------
    /**
     * @return the filePath
     */
    public String getFilePath()
    {
        return filePath;
    }

    //--------------------------------------------------------------------------
    /**
     * @param filePath the filePath to set
     */
    public void setFilePath( String filePath )
    {
        this.filePath = filePath;
    }

    //--------------------------------------------------------------------------
    /**
     * @return the templateFields
     */
    public List<DocumentTemplateField> getTemplateFields()
    {
        return templateFields;
    }

    //--------------------------------------------------------------------------
    /**
     * @param templateFields the templateFields to set
     */
    public void setTemplateFields( List<DocumentTemplateField> templateFields )
    {
        this.templateFields = templateFields;
    }

    //--------------------------------------------------------------------------
    /**
     * @param aField
     */
    public void addTemplateField( DocumentTemplateField aField )
    {
        aField.setTemplate( this );
        templateFields.add( aField );
    }

    //--------------------------------------------------------------------------
    /**
     * @param aField
     */
    public void removeTemplateField( DocumentTemplateField aField )
    {
        templateFields.remove( aField );
        aField.setTemplate( null );
    }

}
