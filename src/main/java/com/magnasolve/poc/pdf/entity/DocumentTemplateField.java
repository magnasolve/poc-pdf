/**
 * =============================================================================
 * Copyright(c) MagnaSolve LLC, 2019.
 * All rights reserved. 
 * =============================================================================
 */

package com.magnasolve.poc.pdf.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

//------------------------------------------------------------------------------
/**
 * @author davidteagle
 *
 */
//------------------------------------------------------------------------------
@Entity
@Table(name = "document_template_field")
public class DocumentTemplateField
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "template_id")
    private DocumentTemplate template;
    
    private String fieldName;
    private String defaultValue;
    
    //--------------------------------------------------------------------------
    /**
     */
    public DocumentTemplateField()
    {
        super();
    }

    //--------------------------------------------------------------------------
    /**
     * @param fieldName
     * @param defaultValue
     */
    public DocumentTemplateField( String fieldName, String defaultValue )
    {
        super();
        this.fieldName = fieldName;
        this.defaultValue = defaultValue;
    }

    //--------------------------------------------------------------------------
    /**
     * @return the id
     */
    public Long getId()
    {
        return id;
    }

    //--------------------------------------------------------------------------
    /**
     * @param id the id to set
     */
    public void setId( Long id )
    {
        this.id = id;
    }

    //--------------------------------------------------------------------------
    /**
     * @return the template
     */
    public DocumentTemplate getTemplate()
    {
        return template;
    }

    //--------------------------------------------------------------------------
    /**
     * @param template the template to set
     */
    public void setTemplate( DocumentTemplate template )
    {
        this.template = template;
    }

    //--------------------------------------------------------------------------
    /**
     * @return the fieldName
     */
    public String getFieldName()
    {
        return fieldName;
    }

    //--------------------------------------------------------------------------
    /**
     * @param fieldName the fieldName to set
     */
    public void setFieldName( String fieldName )
    {
        this.fieldName = fieldName;
    }

    //--------------------------------------------------------------------------
    /**
     * @return the defaultValue
     */
    public String getDefaultValue()
    {
        return defaultValue;
    }

    //--------------------------------------------------------------------------
    /**
     * @param defaultValue the defaultValue to set
     */
    public void setDefaultValue( String defaultValue )
    {
        this.defaultValue = defaultValue;
    }
}
