/**
 * =============================================================================
 * Copyright(c) MagnaSolve LLC, 2019.
 * All rights reserved. 
 * =============================================================================
 */
package com.magnasolve.poc.pdf.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

//------------------------------------------------------------------------------
/**
 * @author dteagle
 *
 */
//------------------------------------------------------------------------------
@Entity
public class SimpleCustomer
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String address1;
    private String address2;
    private String city;
    private String state;
    //--------------------------------------------------------------------------
    /**
     * @return the id
     */
    public Long getId()
    {
        return id;
    }
    //--------------------------------------------------------------------------
    /**
     * @param id the id to set
     */
    public void setId( Long id )
    {
        this.id = id;
    }
    //--------------------------------------------------------------------------
    /**
     * @return the name
     */
    public String getName()
    {
        return name;
    }
    //--------------------------------------------------------------------------
    /**
     * @param name the name to set
     */
    public void setName( String name )
    {
        this.name = name;
    }
    //--------------------------------------------------------------------------
    /**
     * @return the address1
     */
    public String getAddress1()
    {
        return address1;
    }
    //--------------------------------------------------------------------------
    /**
     * @param address1 the address1 to set
     */
    public void setAddress1( String address1 )
    {
        this.address1 = address1;
    }
    //--------------------------------------------------------------------------
    /**
     * @return the address2
     */
    public String getAddress2()
    {
        return address2;
    }
    //--------------------------------------------------------------------------
    /**
     * @param address2 the address2 to set
     */
    public void setAddress2( String address2 )
    {
        this.address2 = address2;
    }
    //--------------------------------------------------------------------------
    /**
     * @return the city
     */
    public String getCity()
    {
        return city;
    }
    //--------------------------------------------------------------------------
    /**
     * @param city the city to set
     */
    public void setCity( String city )
    {
        this.city = city;
    }
    //--------------------------------------------------------------------------
    /**
     * @return the state
     */
    public String getState()
    {
        return state;
    }
    //--------------------------------------------------------------------------
    /**
     * @param state the state to set
     */
    public void setState( String state )
    {
        this.state = state;
    }
}
