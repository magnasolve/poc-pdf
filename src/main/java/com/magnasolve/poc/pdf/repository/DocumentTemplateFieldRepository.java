/**
 * =============================================================================
 * Copyright(c) MagnaSolve LLC, 2019.
 * All rights reserved. 
 * =============================================================================
 */

package com.magnasolve.poc.pdf.repository;

import org.springframework.data.repository.CrudRepository;

import com.magnasolve.poc.pdf.entity.DocumentTemplateField;

//------------------------------------------------------------------------------
/**
 * @author davidteagle
 *
 */
//------------------------------------------------------------------------------

public interface DocumentTemplateFieldRepository extends CrudRepository<DocumentTemplateField, Long>
{

}
