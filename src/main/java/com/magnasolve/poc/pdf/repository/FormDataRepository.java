/**
 * =============================================================================
 * Copyright(c) MagnaSolve LLC, 2019.
 * All rights reserved. 
 * =============================================================================
 */
package com.magnasolve.poc.pdf.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.magnasolve.poc.pdf.entity.FormData;

//------------------------------------------------------------------------------
/**
 * @author dteagle
 *
 */
//------------------------------------------------------------------------------
@Repository
public interface FormDataRepository extends CrudRepository<FormData, Long>
{

    public Optional<FormData> findDistinctByDataOwnerIdAndTemplateId( Long dataOwnerId, Long templateId );
}
