/**
 * =============================================================================
 * Copyright(c) MagnaSolve LLC, 2019.
 * All rights reserved. 
 * =============================================================================
 */
package com.magnasolve.poc.pdf.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.magnasolve.poc.pdf.entity.SimpleCustomer;

//------------------------------------------------------------------------------
/**
 * @author dteagle
 *
 */
//------------------------------------------------------------------------------
@Repository
public interface SimpleCustomerRepository extends CrudRepository<SimpleCustomer, Long>
{

}
