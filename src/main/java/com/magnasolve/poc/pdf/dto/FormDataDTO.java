/**
 * =============================================================================
 * Copyright(c) MagnaSolve LLC, 2019.
 * All rights reserved. 
 * =============================================================================
 */

package com.magnasolve.poc.pdf.dto;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonGetter;

//------------------------------------------------------------------------------
/**
 * DTO object used to serialize and deserialize JSON form data.
 * 
 * @author davidteagle
 */
//------------------------------------------------------------------------------

public class FormDataDTO
{
    private Long dataOwnerId;

    private Long templateId;

    private Map<String, String> formDataMap = new HashMap<String, String>();

    //--------------------------------------------------------------------------
    /**
     * @return the dataOwnerId
     */
    @JsonGetter( value="owner-id")
    public Long getDataOwnerId()
    {
        return dataOwnerId;
    }

    //--------------------------------------------------------------------------
    /**
     * @param dataOwnerId the dataOwnerId to set
     */
    public void setDataOwnerId( Long dataOwnerId )
    {
        this.dataOwnerId = dataOwnerId;
    }

    //--------------------------------------------------------------------------
    /**
     * @return the templateId
     */
    @JsonGetter( value="template-id")
    public Long getTemplateId()
    {
        return templateId;
    }

    //--------------------------------------------------------------------------
    /**
     * @param templateId the templateId to set
     */
    public void setTemplateId( Long templateId )
    {
        this.templateId = templateId;
    }

    //--------------------------------------------------------------------------
    /**
     * @return the formDataMap
     */
    @JsonGetter( value="form-data-map")
    public Map<String, String> getFormDataMap()
    {
        return formDataMap;
    }

    //--------------------------------------------------------------------------
    /**
     * @param formDataMap the formDataMap to set
     */
    public void setFormDataMap( Map<String, String> formDataMap )
    {
        this.formDataMap = formDataMap;
    }
}
