/**
 * =============================================================================
 * Copyright(c) MagnaSolve LLC, 2019.
 * All rights reserved. 
 * =============================================================================
 */
package com.magnasolve.poc.pdf.tools;

import java.io.IOException;
import java.util.Map;

import org.apache.pdfbox.pdmodel.PDDocument;

//------------------------------------------------------------------------------
/**
 * @author davidteagle
 *
 */
//------------------------------------------------------------------------------

public class PdfTools
{

    //--------------------------------------------------------------------------
    /**
     */
    public PdfTools()
    {
        super();
        // TODO Auto-generated constructor stub
    }

    //--------------------------------------------------------------------------
    /**
     * @param args
     */
    public static void main( String[] args )
    {
        if( args != null && args.length > 0 )
        {
            PdfTools tools = new PdfTools();
            
            switch(args[0])
            {
            case "buildForm1":
                tools.doCreateForm( "TestTemplate1", PdfToolsUtil.FIELDS_1 );
                break;
            case "buildForm2":
                tools.doCreateForm( "TestTemplate2", PdfToolsUtil.FIELDS_2 );
                break;
            default:
                System.out.println( "Unknown argument." );
            }
        }
        else
        {
            System.out.println( "No argument." );
        }
    }

    //--------------------------------------------------------------------------
    /**
     */
    public void doCreateForm(String formName, Map<String,String> fields )
    {
        PDDocument document = null;
        try 
        {
            document = PdfToolsUtil.buildSimpleForm( fields );
            document.save( formName + ".pdf" );
        } 
        catch (IOException e) 
        {
            System.out.print( "ERROR: " + e.getLocalizedMessage() );
        }
    
    }
}
