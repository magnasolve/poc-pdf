/**
 * =============================================================================
 * Copyright(c) MagnaSolve LLC, 2019.
 * All rights reserved. 
 * =============================================================================
 */

package com.magnasolve.poc.pdf.tools;

import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.PDPageContentStream.AppendMode;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.color.PDColor;
import org.apache.pdfbox.pdmodel.graphics.color.PDDeviceRGB;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotationWidget;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAppearanceCharacteristicsDictionary;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDTextField;

//------------------------------------------------------------------------------
/**
 * @author davidteagle
 *
 */
//------------------------------------------------------------------------------

public class PdfToolsUtil
{
    public static final String FORM_SIMPLE = "simple";
    public static final String FORM_MULTIPAGE = "multi";
    
    public static final String DEFAULT_PATH="target/simpleform.pdf";
    
    public static final Map<String,String> FIELDS_1 = Map.of(
        "NameField",     "Enter name:", 
        "Address1Field", "Enter address 1:",
        "Address2Field", "Enter address 2:",
        "CityField",     "Enter city:",
        "StateField",    "Enter state:" 
    );
    
    public static final Map<String,String> FIELDS_2 = Map.of(
        "NameField",    "Enter name:",
        "DateField",    "Enter date:",
        "SSNField",     "Enter SSN:",
        "IncomeField",  "Enter income:",
        "CommentField", "Enter comments:"
    );
    
    public static void generateTestDocument(String filePath, String formType) throws IOException
    {
//        if(filePath == null) filePath = DEFAULT_PATH;
//        
//        PDDocument testDoc = null;
//        
//        if(FORM_SIMPLE.equalsIgnoreCase(formType)) testDoc = buildSimpleForm();
//        else if(FORM_MULTIPAGE.equalsIgnoreCase(formType)) testDoc = buildSimpleForm();
//        else testDoc = buildSimpleForm();
//        
//        testDoc.save(filePath);
        
    }
    
    public static PDDocument buildSimpleForm( Map<String,String> fieldMap ) throws IOException
    {
        PDDocument document = new PDDocument();
        
        PDPage page = new PDPage(PDRectangle.LETTER);
        document.addPage(page);
        
//        createSimpleLabels(document, page, fieldMap);
        // Adobe Acrobat uses Helvetica as a default font and
        // stores that under the name '/Helv' in the resources dictionary
        PDFont font = PDType1Font.HELVETICA;
        PDResources resources = new PDResources();
        resources.put(COSName.getPDFName("Helv"), font);

        // Add a new AcroForm and add that to the document
        PDAcroForm acroForm = new PDAcroForm(document);
        document.getDocumentCatalog().setAcroForm(acroForm);

        // Add and set the resources and default appearance at the form level
        acroForm.setDefaultResources(resources);

        // Acrobat sets the font size on the form level to be
        // auto sized as default. This is done by setting the font size to '0'
        String defaultAppearanceString = "/Helv 0 Tf 0 g";
        acroForm.setDefaultAppearance(defaultAppearanceString);
        
        int startY = 700;
        for( Entry<String, String> entry : fieldMap.entrySet() )
        {  
            createLabel( document, page, startY, entry.getValue() );
            createTextField( entry.getKey(), startY, acroForm, page );
            startY = startY - 40;
        }
//        createTextField( "NameField", 700, acroForm, page );
//        createTextField( "Address1Field", 660, acroForm, page );
//        createTextField( "Address2Field", 620, acroForm, page );
//        createTextField( "CityField", 580, acroForm, page );
//        createTextField( "StateField", 540, acroForm, page );
            
        return document;
    }
    
    public static void createLabel( PDDocument doc, PDPage page, 
                                    Integer yVal, String labelText )
    {

        PDFont font = PDType1Font.HELVETICA;

        PDPageContentStream contents;
        try
        {
            contents = new PDPageContentStream(doc, page, AppendMode.APPEND, true, false);
            contents.beginText();
            contents.setFont(font, 12);
            
            contents.newLineAtOffset(50, yVal+10 );
            contents.showText(labelText);
            contents.endText();
            contents.close();
        } 
        catch ( IOException e )
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    
    public static void createTextField( String name, float yVal, PDAcroForm acroForm, PDPage page )
    {
     // Add a form field to the form.
        PDTextField textBox = new PDTextField(acroForm);
        textBox.setPartialName( name );

        // Acrobat sets the font size to 12 as default
        // This is done by setting the font size to '12' on the
        // field level.
        // The text color is set to blue in this example.
        // To use black, replace "0 0 1 rg" with "0 0 0 rg" or "0 g".
        textBox.setDefaultAppearance("/Helv 12 Tf 0 0 1 rg");

        // add the field to the acroform
        acroForm.getFields().add(textBox);

        // Specify the annotation associated with the field
        PDAnnotationWidget widget = textBox.getWidgets().get(0);
        PDRectangle rect = new PDRectangle(200, yVal, 250, 25);
        widget.setRectangle(rect);
        widget.setPage(page);

        // set black border and yellow background
        // if you prefer defaults, just delete this code block
        PDAppearanceCharacteristicsDictionary fieldAppearance = 
            new PDAppearanceCharacteristicsDictionary(new COSDictionary());
        
        fieldAppearance.setBorderColour(new PDColor(new float[] { 0, 0, 0 }, PDDeviceRGB.INSTANCE));
//      fieldAppearance.setBackground(new PDColor(new float[] { 1, 1, 0 }, PDDeviceRGB.INSTANCE));
        widget.setAppearanceCharacteristics(fieldAppearance);

        // make sure the annotation is visible on screen and paper
        widget.setPrinted(true);
        
        // Add the annotation to the page
        try
        {
            page.getAnnotations().add(widget);
        } catch ( IOException e )
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
