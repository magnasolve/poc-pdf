/**
 * =============================================================================
 * Copyright(c) MagnaSolve LLC, 2019.
 * All rights reserved. 
 * =============================================================================
 */

package com.magnasolve.poc.pdf.util;

import java.util.List;
import java.util.Map;

import com.magnasolve.poc.pdf.dto.FormDataDTO;
import com.magnasolve.poc.pdf.entity.FormData;
import com.magnasolve.poc.pdf.entity.FormDataEntry;

//------------------------------------------------------------------------------
/**
 * Utility methods for form data objects.
 * 
 * @author davidteagle
 */
//------------------------------------------------------------------------------

public class FormUtil
{

    //--------------------------------------------------------------------------
    /**
     * Converts a JSON form DTO to a FormData entity.
     * @param dto the JSON dto
     * @return the created FormData object
     */
    public static FormData convertFormDataDTO( FormDataDTO dto )
    {
        FormData data = new FormData();
        data.setDataOwnerId( dto.getDataOwnerId() );
        data.setTemplateId( dto.getTemplateId() );
        
        Map<String,String> dataMap = dto.getFormDataMap();
        
        dataMap.forEach(( key, value ) -> 
        {
            FormDataEntry entry = new FormDataEntry( key, value );
            data.addFormDataEntry( entry );
        });
        return data;
    }
    
    //--------------------------------------------------------------------------
    /**
     * Converts a FormData entity into a JSON DTO.
     * @param formData the entity
     * @return the dto
     */
    public static FormDataDTO convertFormData( FormData formData )
    {
        FormDataDTO dto = new FormDataDTO();
        dto.setDataOwnerId( formData.getDataOwnerId() );
        dto.setTemplateId( formData.getTemplateId() );
        
        Map<String,String> dataMap = dto.getFormDataMap();
        List<FormDataEntry> entries = formData.getFormDataEntries();
        
        entries.forEach( entry -> 
        {
            dataMap.put( entry.getEntryKey(), entry.getEntryValue() );
        });     
        return dto;
    }
}
