/**
 * =============================================================================
 * Copyright(c) MagnaSolve LLC, 2019.
 * All rights reserved. 
 * =============================================================================
 */
package com.magnasolve.poc.pdf.util;

import java.util.Optional;

import com.magnasolve.poc.pdf.entity.DocumentTemplate;
import com.magnasolve.poc.pdf.entity.DocumentTemplateField;

//------------------------------------------------------------------------------
/**
 * @author davidteagle
 *
 */
//------------------------------------------------------------------------------

public class TemplateUtil
{
    //--------------------------------------------------------------------------
    /**
     * @param name
     * @param template
     * @return
     */
    public static boolean verifyFieldName( String name, DocumentTemplate template )
    {
        Optional<DocumentTemplateField> match = 
            template.getTemplateFields().stream().filter( 
                field -> field.getFieldName().equals( name )).findFirst();
        
        return match.isPresent();
    }
}
