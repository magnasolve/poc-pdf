/**
 * =============================================================================
 * Copyright(c) MagnaSolve LLC, 2019.
 * All rights reserved. 
 * =============================================================================
 */

package com.magnasolve.poc.pdf.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.fdf.FDFDocument;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import org.apache.pdfbox.pdmodel.interactive.form.PDTextField;

import com.magnasolve.poc.pdf.entity.FormData;

//------------------------------------------------------------------------------
/**
 * Utility methods for PDF document manipulation.
 * 
 * @author davidteagle
 */
//------------------------------------------------------------------------------
public class PdfUtil 
{
	
	//--------------------------------------------------------------------------
	/**
	 * Maps a FormData object into corresponding fields in a PDF document.
	 * @param document - the PDF document
	 * @param data - the form data
	 * @return - the merged PDF document
	 */
	public static PDDocument generateMergedForm( PDDocument document, FormData data )
	{
	    // get the document catalog
        PDAcroForm acroForm = document.getDocumentCatalog().getAcroForm();
        
        if( acroForm != null )
        {
            data.getFormDataEntries().forEach(( entry ) -> 
            {
                PDTextField field = (PDTextField)acroForm.getField( entry.getEntryKey() );
                if( field != null )
                {
                    try
                    {
                        field.setValue( entry.getEntryValue() );
                    } 
                    catch ( IOException e )
                    {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            });
            return document;
        }
	    return null;
	}
	
	//--------------------------------------------------------------------------
	/**
	 * @param document
	 * @return
	 */
	public static FDFDocument getFDF( PDDocument document )
	{
	    FDFDocument fdf = null;
	    
	    // get the AcroForm
        PDAcroForm acroForm = document.getDocumentCatalog().getAcroForm();
        
        if( acroForm != null )
        {
            try
            {
                fdf = acroForm.exportFDF();
            } 
            catch ( IOException e )
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return fdf;
	}
	
	//--------------------------------------------------------------------------
	/**
	 * @param document
	 * @return
	 */
	public static List<PDField> getPdfFields( PDDocument document )
	{
	    List<PDField> fields = null;
	    // get the AcroForm
        PDAcroForm acroForm = document.getDocumentCatalog().getAcroForm();
        
        if( acroForm != null )
        {
            fields = acroForm.getFields();
        }
	    return fields;
	}
	
	//--------------------------------------------------------------------------
	/**
	 * @param document
	 * @return
	 */
	public static List<String> getPdfFieldNames( PDDocument document )
	{
	    List<String> nameList = new ArrayList<String>();
	    
	    List<PDField> fields = getPdfFields( document );
	    
	    fields.forEach( field -> { nameList.add( field.getPartialName()); });
	    
	    return nameList;
	}
	
	//--------------------------------------------------------------------------
	/**
	 * @param stream
	 * @return
	 */
	public static PDDocument documentFromStream( InputStream stream )
	{
	    PDDocument document = null;
	    try
        {
            document = PDDocument.load( stream );
        } 
	    catch ( Exception e )
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
	    return document;
	}
}
