/**
 * =============================================================================
 * Copyright(c) MagnaSolve LLC, 2019.
 * All rights reserved. 
 * =============================================================================
 */

package com.magnasolve.poc.pdf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

//------------------------------------------------------------------------------
/**
 * Main application class for PDF POC
 * 
 * @author davidteagle
 */
//------------------------------------------------------------------------------
@SpringBootApplication
@ComponentScan(basePackages = { "com.magnasolve.poc" } )
public class PocPdfApplication 
{

	//--------------------------------------------------------------------------
	/**
	 * Entry point.
	 * 
	 * @param args
	 */
	public static void main( String[] args ) 
	{
		SpringApplication.run( PocPdfApplication.class, args );
	}

}
