/**
 * =============================================================================
 * Copyright(c) MagnaSolve LLC, 2019.
 * All rights reserved. 
 * =============================================================================
 */

package com.magnasolve.poc.controller.pdf;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.magnasolve.poc.pdf.dto.FormDataDTO;
import com.magnasolve.poc.pdf.entity.DocumentTemplate;
import com.magnasolve.poc.pdf.entity.FormData;
import com.magnasolve.poc.pdf.service.PdfService;
import com.magnasolve.poc.pdf.util.FormUtil;
import com.magnasolve.poc.service.form.FormService;
import com.magnasolve.poc.service.template.TemplateService;

//------------------------------------------------------------------------------
/**
 * API for the PDF POC.
 * 
 * @author davidteagle
 */
//------------------------------------------------------------------------------
@RestController
public class PdfRestController 
{
    @Autowired
    private PdfService pdfService;
    
    @Autowired
    private FormService formService;
    
    @Autowired
    private TemplateService templateService;
    
	//--------------------------------------------------------------------------
	/**
	 * Generates a PDF document from form data stored in the database.
	 * @param id - the form id
	 * @param ownerId - the owner id (customer)
	 * @return path to generated PDF document
	 */
	@GetMapping( "/pdf/{id}/owners/{ownerId}" )
    public String getMergedPdf( @PathVariable Long id, 
                                @PathVariable Long ownerId ) 
	{
	    //TODO: this should probably return an URI instead of a file path
	    return pdfService.generatePdf( ownerId, id );
    }
	
	//--------------------------------------------------------------------------
	/**
	 * Saves the form data from a PDF file.
	 * @param formId id of the form template to use
	 * @param ownerId the owner id (customer)
	 * @param fileName name of the uploaded file
	 * @return
	 */
	@PutMapping( "/pdf" )
	public String savePdf( @RequestParam( "form-id" ) Long formId, 
	                       @RequestParam( "owner-id" ) Long ownerId,  
	                       @RequestParam( "file" ) MultipartFile file )
	{
	    FormData data = new FormData( ownerId, formId );      
	    try
        {
            data = pdfService.loadPdfFormData( data, file.getInputStream() );
            return "PDF form saved.";
        } 
	    catch ( IOException e )
        {
            return "error: " + e.getLocalizedMessage();
        }
	}
	
	//--------------------------------------------------------------------------
	/**
	 * Persists the data for a PDF form.
	 * @param dto - a deserialized JSON object containing form data
	 */
	@PutMapping( "/forms" )
	public String saveFormData( @RequestBody FormDataDTO dto )
	{
	    FormData data = FormUtil.convertFormDataDTO( dto );
	    formService.saveFormData( data );
	    return "Form data saved.";
	}

	//--------------------------------------------------------------------------
    /**
     * Returns a JSON representation of form data stored in the database.
     * @param id - the form id
     * @param ownerId - the owner id (customer)
     * @return JSON object with name-value pairs of form data
     */
	@GetMapping( "/forms/{id}/owners/{ownerId}" )
	public FormDataDTO getFormData (@PathVariable Long id, 
                                    @PathVariable Long ownerId )
	{
	    FormData formData = formService.getFormData( ownerId, id );
	    
	    if( formData != null )
	    {
	        return FormUtil.convertFormData( formData );
	    }
	    return null;
	}
	//--------------------------------------------------------------------------
	/**
	 * Uploads a PDF file and builds a template from the fields in the file.
	 * @param templateName
	 * @param fileName
	 */
	@PutMapping("/templates")
	public String saveTemplate( @RequestParam( "name" ) String templateName,
	                            @RequestParam( "file" ) MultipartFile file )
	{
	    DocumentTemplate template = null;
	    
	    try
        {
            template = templateService.addTemplateFromFile( templateName, file.getInputStream() );
            return "Template saved with id - " + template.getId();
        } 
	    catch ( Exception e )
        {
            return "error: " + e.getLocalizedMessage();
        }  
	}
	
	//--------------------------------------------------------------------------
	/**
	 * Returns a xml version of a template.
	 * @param id
	 * @return
	 */
	@GetMapping("/templates/{id}")
	public String getTemplate( @PathVariable Long id )
	{
	    return pdfService.getXfdfTemplate( id );
	}
}
