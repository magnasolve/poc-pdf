/**
 * 
 */
package com.magnasolve.poc.pdf.util;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileWriter;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.fdf.FDFDocument;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.junit.Test;

/**
 * @author dteagle
 *
 */
public class PdfUtilTest {

//    public static final String testPath = "/Users/davidteagle/test/templates/pdf/";
    public static final String testPath = "/Users/davidteagle/test/output/pdf/";
    
	/**
	 * Test method for {@link com.magnasolve.poc.pdf.util.PdfUtil#generateMergedForm()}.
	 */
//	@Test
//	public final void testGenerateMergedForm() 
//	{
//		PDDocument document = null;
//        try
//        {
//            document = PDDocument.load( new File("/Users/davidteagle/test/templates/pdf/1001.pdf" ));
//        } 
//        catch ( Exception e )
//        {
//            fail(e.getMessage());
//        }	
//		assertNotNull("PDF document is null", document);
//	}
	
	/**
	 * Test method for {@link com.magnasolve.poc.pdf.util.PdfUtil#getFDF(java.lang.String)}.
	 */
//	@Test
//	public final void testGetFDF() 
//	{
//	    try
//        {
////            doTestFDF( "1001" );
//            doTestFDF( "form-1001-1001" );
//            doTestFDF( "20-14_0" );
//        } 
//        catch ( Exception e )
//        {
//            fail(e.getMessage());
//        }
//        
//	}

	private final void doTestFDF( String templateName ) throws Exception
	{
        PDDocument document = PDDocument.load( 
	        new File( testPath + templateName + ".pdf" ));
	    
        assertNotNull("PDF document is null", document);
        
        PDAcroForm acroForm = document.getDocumentCatalog().getAcroForm();
        assertNotNull("PDF acroForm is null", acroForm);
        
        FDFDocument fdf = acroForm.exportFDF();
        assertNotNull("FDF document is null", fdf);
        
        fdf.save( templateName + ".fdf" );
        fdf.saveXFDF( templateName + ".xfdf" );
        
        fdf.writeXML( new FileWriter( templateName + ".xml" ));
	}


}
