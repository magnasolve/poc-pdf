/**
 * =============================================================================
 * Copyright(c) MagnaSolve LLC, 2019.
 * All rights reserved. 
 * =============================================================================
 */
package com.magnasolve.poc.pdf.entity;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.magnasolve.poc.pdf.entity.FormData;
import com.magnasolve.poc.pdf.repository.FormDataRepository;

//------------------------------------------------------------------------------
/**
 * @author dteagle
 *
 */
//------------------------------------------------------------------------------
@RunWith(SpringRunner.class)
@DataJpaTest
public class FormDataRepositoryTest
{
    @Autowired
    private FormDataRepository repository;
    
    //--------------------------------------------------------------------------
    /**
     */
    @Test
    public void findByIdTest()
    {
        Optional<FormData> formData = repository.findById( (long)1 );
        assertThat( formData.isPresent() );
        assertThat( formData.get().getId() ).isEqualTo( (long)1 );
        assertThat( formData.get().getDataOwnerId() ).isEqualTo( (long)1001);
        
    }
    
    @Test
    public void findByOwnerAndTemplateTest()
    {
        Optional<FormData> formData = repository.findDistinctByDataOwnerIdAndTemplateId( 1001l, 1001l );
        assertThat( formData.isPresent() );
    }
}
