/**
 * =============================================================================
 * Copyright(c) MagnaSolve LLC, 2019.
 * All rights reserved. 
 * =============================================================================
 */
package com.magnasolve.poc.pdf.entity;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.magnasolve.poc.pdf.entity.SimpleCustomer;
import com.magnasolve.poc.pdf.repository.SimpleCustomerRepository;

//------------------------------------------------------------------------------
/**
 * @author dteagle
 *
 */
//------------------------------------------------------------------------------
@RunWith(SpringRunner.class)
@DataJpaTest
public class SimpleCustomerRepositoryTest
{

    @Autowired
    private SimpleCustomerRepository repository;
    
    //--------------------------------------------------------------------------
    /**
     */
    @Test
    public void findByIdTest()
    {
        Optional<SimpleCustomer> template = repository.findById( (long) 1001 );
        assertThat(template.isPresent());
        assertThat(template.get().getId()).isEqualTo( (long )1001);
    }
}
